package Ro.Orange;

public class Receptionists extends EmployeeSalaries {
    private int salary = 1200;

    private int getSalary() {
        return salary;
    }

    public String toString() {
        return "The salary of Receptionists is somewhere around " + getSalary();
    }
}
