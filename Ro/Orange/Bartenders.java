package Ro.Orange;

public class Bartenders extends EmployeeSalaries {
    private int salary = 3200;

    private int getSalary() {
        return salary;
    }

    public String toString() {
        return "The salary of Receptionists is somewhere around " + getSalary();
    }
}
