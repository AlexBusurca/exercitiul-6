package Ro.Orange;

public class Maids extends EmployeeSalaries {
    private int salary = 2200;

    private int getSalary() {
        return salary;
    }

    public String toString() {
        return "The salary of Maids is somewhere around " + getSalary();
    }
}
